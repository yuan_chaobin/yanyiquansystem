import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import ModelLibrary from '../views/RecruitmentManage/ModelLibrary.vue'
import RecruitmentCompany from '../views/RecruitmentManage/RecruitmentCompany.vue'
import UpdateUserRank from '../views/RecruitmentManage/UpdateUserRank.vue'
import PhotosAlbum from '../views/RecruitmentManage/PhotosAlbum.vue'
import SerachCompany from '../views/RecruitmentManage/SerachCompany.vue'
import SerachAssistant from '../views/RecruitmentManage/SerachAssistant.vue'
import CompanyDeails from '../views/RecruitmentManage/CompanyDeails.vue'
import ModelsDeails from '../views/RecruitmentManage/ModelsDeails.vue'
import FirstPageModel from '../views/RecruitmentManage/FirstPageModel.vue'
import Assistant from '../views/RecruitmentManage/Assistant.vue'
import EditAssistant from '../views/RecruitmentManage/EditAssistant.vue'
import AssistantModels from '../views/RecruitmentManage/AssistantModels.vue'
import UserRights from '../views/RecruitmentManage/UserRights.vue'
import SerachUserRights from '../views/RecruitmentManage/SerachUserRights.vue'
import particularsMT from '../views/Authentication/particulars_MT.vue'
import AddMT from '../views/Authentication/Add_MT.vue'
/* import { component } from 'vue/types/umd' */
import Menu from '../views/Menu.vue'
import UsefulExpressionsManager from '../views/UsefulExpressionsManager.vue'
import Login from '../views/Login.vue'
import SensibilityManager from '../views/SensibilityManager.vue'
import MenuSystem from '../views/MenuSystem.vue'
import AddMenu from '../views/AddMenu.vue'
import AddSensibilityManager from '../views/AddSensibilityManager.vue'
import AddUsefulExpressionsManager from '../views/AddUsefulExpressionsManager.vue'
import EditUsefulExpressionsManager from '../views/EditUsefulExpressionsManager.vue'
import EditSensibilityManager from '../views/EditSensibilityManager.vue'
import EditMenu from '../views/EditMenu.vue'
import Note from '../views/Note.vue'
import AddGS from '../views/Authentication/Add_GS.vue'
import particularsGS from '../views/Authentication/particulars_GS.vue'//公司的详情
import offlinemt from '../views/Authentication/offline_MT.vue'//线下模特
import offlinemtzl from '../views/Authentication/offline_ZL.vue'//线下模特
import offlinemtgs from '../views/Authentication/offline_GS.vue'//线下公司
import Addrecruitment from '../views/Auditrecruitment/Addrecruitment.vue'//添加招聘信息
import showrecruitment from '../views/Auditrecruitment/Auditrecruitment.vue'//显示认证招聘信息
import ShowAuditrecruitment from '../views/RecruitmentManage/ShowAuditrecruitment.vue'//显示招聘庫
import particulaAud from '../views/RecruitmentManage/particula_Aud.vue'//查看详情招聘库

import PurchaseRecord from '../views/RecruitmentManage/PurchaseRecord.vue'//用户权益购买记录
import IdentitySwitch from '../views/RecruitmentManage/IdentitySwitch.vue'//身份切换
import { component } from 'vue/types/umd'
import Label from '../views/Label/Label.vue'
import AddQuanYiWeiHu from '../views/xitongquanyiweihu/AddQuanYiWeiHu.vue'
import QuanYiWeiHu from '../views/xitongquanyiweihu/QuanYiWeiHu.vue'
import EditQuanYiWeiHu from '../views/xitongquanyiweihu/EditQuanYiWeiHu.vue'
import AddsystemInfrom from '../views/xitongtongzhi/AddsystemInfrom.vue'
import SystemInfrom from '../views/xitongtongzhi/SystemInfrom.vue'
import XuanTui from '../views/xitongtongzhi/XuanTui.vue'
import UpdateSystemInfrom from '../views/xitongtongzhi/UpdateSystemInfrom.vue'
import YongHuJinYan from '../views/yonghujinyan/YongHuJinYan.vue'




Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta:{
      needLogin: true
    }

  },
  {//身份切换
    path: '/RecruitmentManage/IdentitySwitch',
    name: 'IdentitySwitch',
    component: IdentitySwitch , meta:{
      needLogin: true
    }

  },
  {//用户权益购买记录
    path: '/RecruitmentManage/PurchaseRecord',
    name: 'PurchaseRecord',
    component: PurchaseRecord , meta:{
      needLogin: true
    }

  },
  {//用户禁言
    path: '/yonghujinyan/YongHuJinYan',
    name: 'YongHuJinYan',
    component: YongHuJinYan , meta:{
      needLogin: true
    }

  },
  {//登录
    path: '/Login',
    name: 'Login',
    component: Login
    , meta:{
      needLogin: true
    }

  },
  {//模特列表
    path: '/ModelLibrary',
    name: 'Model',
    component: ModelLibrary
    , meta:{
      needLogin: true
    }
  },
  //招聘公司
  {
    path: '/RecruitmentCompany',
    name: 'Company',
    component: RecruitmentCompany , meta:{
      needLogin: true
    }
  },
  //首页模特
  {
    path: '/FirstPageModel',
    name: 'PageModel',
    component: FirstPageModel , meta:{
      needLogin: true
    }
  },
  //模特详情
  {
    path: '/ModelsDeails',
    name: 'details',
    component: ModelsDeails , meta:{
      needLogin: true
    }
  },
  //修改等级
  {
    path: '/UpdateUserRank',
    name: 'rank',
    component: UpdateUserRank , meta:{
      needLogin: true
    }
  },
  //相册审核
  {
    path: '/PhotosAlbum',
    name: 'photo',
    component: PhotosAlbum , meta:{
      needLogin: true
    }
  },
  //查询公司
  {
    path: '/SerachCompany',
    name: 'company',
    component: SerachCompany , meta:{
      needLogin: true
    }
  },
  //查询助理
  {
    path: '/SerachAssistant',
    name: 'SeaAssis',
    component: SerachAssistant , meta:{
      needLogin: true
    }
  },
   //修改助理
   {
    path: '/EditAssistant',
    name: 'editAss',
    component: EditAssistant , meta:{
      needLogin: true
    }
  },
   //助理下的模特
   {
    path: '/AssistantModels',
    name: 'asModel',
    component: AssistantModels , meta:{
      needLogin: true
    }
  },
  //公司详情
  {
    path: '/CompanyDeails',
    name: 'comde',
    component: CompanyDeails , meta:{
      needLogin: true
    }
  },
  //助理列表
  {
    path: '/Assistant',
    name: 'assis',
    component: Assistant , meta:{
      needLogin: true
    }
  },
  //用户权益
  {
    path: '/UserRights',
    name: 'uright',
    component: UserRights , meta:{
      needLogin: true
    }
  },
  //查询用户权益
  {
    path: '/SerachUserRights',
    name: 'Serachright',
    component: SerachUserRights , meta:{
      needLogin: true
    }
  }
  , {//常用语管理
    path: '/UsefulExpressionsManager',
    name: 'UsefulExpressionsManager',
    component: UsefulExpressionsManager , meta:{
      needLogin: true
    }
  },
  {//常用语管理
    path: '/AddUsefulExpressionsManager',
    name: 'AddUsefulExpressionsManager',
    component: AddUsefulExpressionsManager , meta:{
      needLogin: true
    }
  },
  {//常用语管理
    path: '/EditUsefulExpressionsManager',
    name: 'EditUsefulExpressionsManager',
    component: EditUsefulExpressionsManager , meta:{
      needLogin: true
    }
  },
  {
    //菜单主页面
    path: '/MenuSystem',
    name: 'MenuSystem',
    component: MenuSystem , meta:{
      needLogin: true
    }
  },
  {
    //菜单管理
    path: '/Menu',
    name: 'Menu',
    component:Menu , meta:{
      needLogin: true
    }
  }, {
    //菜单管理--添加
    path: '/AddMenu',
    name: 'AddMenu',
    component:AddMenu , meta:{
      needLogin: true
    }
  },
  {
    //菜单管理--添加
    path: '/EditMenu',
    name: 'EditMenu',
    component:EditMenu , meta:{
      needLogin: true
    }
  }
  ,
  {//敏感字管理显示
    path: '/SensibilityManager',
    name: 'SensibilityManager',
    component:SensibilityManager , meta:{
      needLogin: true
    }
  },
   {//敏感字管理添加
    path: '/AddSensibilityManager',
    name: 'AddSensibilityManager',
    component:AddSensibilityManager , meta:{
      needLogin: true
    }
  },
  {//敏感字管理修改
    path: '/EditSensibilityManager',
    name: 'EditSensibilityManager',
    component:EditSensibilityManager , meta:{
      needLogin: true
    }
  },
   {//敏感字管理修改
    path: '/Note',
    name: 'Note',
    component:Note , meta:{
      needLogin: true
    }
  },
  {//标签管理
    path: '/Label/Label',
    name: 'Label',
    component:Label , meta:{
      needLogin: true
    }
  },
   {//系统产品权益添加
    path: '/xitongquanyiweihu/AddQuanYiWeiHu',
    name: 'AddQuanYiWeiHu',
    component:AddQuanYiWeiHu , meta:{
      needLogin: true
    }
  },
   {//系统权益产品显示
    path: '/xitongquanyiweihu/QuanYiWeiHu',
    name: 'QuanYiWeiHu',
    component:QuanYiWeiHu , meta:{
      needLogin: true
    }
  },
   {//系统权益产品修改
    path: '/xitongquanyiweihu/EditQuanYiWeiHu',
    name: 'EditQuanYiWeiHu',
    component:EditQuanYiWeiHu , meta:{
      needLogin: true
    }
  },
    {//系统通知添加
      path: '/xitongtongzhi/AddsystemInfrom',
      name: 'AddsystemInfrom',
      component:AddsystemInfrom , meta:{
        needLogin: true
      }
    },
     {//系统通知显示
      path: '/xitongtongzhi/SystemInfrom',
      name: 'SystemInfrom',
      component:SystemInfrom , meta:{
        needLogin: true
      }
    },
     {//系统通知修改
      path: '/xitongtongzhi/UpdateSystemInfrom',
      name: 'UpdateSystemInfrom',
      component:UpdateSystemInfrom , meta:{
        needLogin: true
      }
    },
    {//系统通知修改
      path: '/xitongtongzhi/XuanTui',
      name: 'XuanTui',
      component:XuanTui
    },
  {
    //活动管理
    path: '/activity',
    name: 'Activity',
    component: () => import(/* webpackChunkName: "about" */ '../views/Activity.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/addActivity',
    name: 'AddActivity',
    component: () => import(/* webpackChunkName: "about" */ '../views/AddActivity.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/activitySerach',
    name: 'ActivitySerach',
    component: () => import(/* webpackChunkName: "about" */ '../views/tsh/ActivitySerach.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/activityreport',
    name: 'Activityreport',
    component: () => import(/* webpackChunkName: "about" */ '../views/tsh/Activityreport.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/Activitycheck',
    name: 'Activitycheck',
    component: () => import(/* webpackChunkName: "about" */ '../views/tsh/Activitycheck.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/activityModel',
    name: 'ActivityModel',
    component: () => import(/* webpackChunkName: "about" */ '../views/tsh/ActivityModel.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/activityreportdelies',
    name: 'Activityreportdelies',
    component: () => import(/* webpackChunkName: "about" */ '../views/tsh/Activityreportdelies.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/activitychecklei',
    name: 'Activitychecklei',
    component: () => import(/* webpackChunkName: "about" */ '../views/tsh/Activitychecklei.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/activitybouna',
    name: 'Activitybouna',
    component: () => import(/* webpackChunkName: "about" */ '../views/tsh/Activitybouna.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/activityfarwe',
    name: 'Activityfarwe',
    component: () => import(/* webpackChunkName: "about" */ '../views/tsh/Activityfarwe.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/activitypototal',
    name: 'Activitypototal',
    component: () => import(/* webpackChunkName: "about" */ '../views/tsh/Activitypototal.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/activitysereport',
    name: 'Activitysereport',
    component: () => import(/* webpackChunkName: "about" */ '../views/tsh/Activitysereport.vue') , meta:{
      needLogin: true
    }
  },
  {
    path: '/editActivity',
    name: 'EditActivity',
    component: () => import(/* webpackChunkName: "about" */ '../views/EditActivity.vue') , meta:{
      needLogin: true
    }
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue') , meta:{
      needLogin: true
    }
  }, {//线上认证模特/助理
    path: '/On-line_MT',
    name: 'Authentication',
    component: () => import(/* webpackChunkName: "about" */ '../views/Authentication/On-line_MT.vue')
    , meta:{
      needLogin: true
    }
  }
  , {//线上认证公司
    path: '/On-line_GS',
    component: () => import(/* webpackChunkName: "about" */ '../views/Authentication/On-line_GS.vue') , meta:{
      needLogin: true
    }
  }, {//员工
    path: '/Emp',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuEmp/Emp.vue') , meta:{
      needLogin: true
    }
  },
  {//模特的详细信息
    path: '/particula_MT',
    component: particularsMT , meta:{
      needLogin: true
    }
  }, {//员工的显示
    path: '/Emps',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuEmp/Emps.vue') , meta:{
      needLogin: true
    }
  }, {
    path: '/AddEmp',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuEmp/AddEmp.vue') , meta:{
      needLogin: true
    }
  },
  {//添加模特
    path: '/addmt',
    component: AddMT , meta:{
      needLogin: true
    }
  }, {//员工的反填修改
    path: '/FanEmp',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuEmp/FanEmp.vue') , meta:{
      needLogin: true
    }
  }
  , {//全局配置
    path: '/GlobalConfig',
    component: () => import(/* webpackChunkName: "about" */ '../views/GlobalConfig.vue') , meta:{
      needLogin: true
    }
  },{//角色的显示
    path: '/Roles',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuRole/Roles.vue') , meta:{
      needLogin: true
    }
  },
  {//角色的添加
    path: '/AddRole',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuRole/AddRole.vue') , meta:{
      needLogin: true
    }
  },{//角色的反填修改
    path: '/FanRole',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuRole/FanRole.vue') , meta:{
      needLogin: true
    }
  },{//员工权限的显示
    path: '/Jurisdiction',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuEmp/Jurisdiction.vue') , meta:{
      needLogin: true
    }
  },{//员工权限的编辑查看
    path: '/FanJurisdiction',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuEmp/FanJurisdiction.vue') , meta:{
      needLogin: true
    }
  },{//Banner维护的显示
    path: '/Banners',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuBanner/Banners.vue') , meta:{
      needLogin: true
    }
  },{//Banner维护的添加
    path: '/AddBanner',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuBanner/AddBanner.vue') , meta:{
      needLogin: true
    }
  },{//Banner维护的反填修改
    path: '/FanBanner',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuBanner/FanBanner.vue') , meta:{
      needLogin: true
    }
  },{//添加权益
    path: '/AddRightsConfiguration',
    component: () => import(/* webpackChunkName: "about" */ '../views/RightsConfiguration/AddRightsConfiguration.vue') , meta:{
      needLogin: true}
    },
  {//权益维护的显示
    path: '/GetRightsConfiguration',
    component: () => import(/* webpackChunkName: "about" */ '../views/RightsConfiguration/GetRightsConfiguration.vue') , meta:{
      needLogin: true}
    } ,{
    //权益维护的修改
    path: '/FanRightsConfiguration',
    component: () => import(/* webpackChunkName: "about" */ '../views/RightsConfiguration/FanRightsConfiguration.vue') , meta:{
      needLogin: true
    }
  }
  ,{
    path: '/Menus',
    component: () => import(/* webpackChunkName: "about" */ '../views/FuRole/Menus.vue'), meta:{
      needLogin: true
    }
  },

{//身份切换
  path: '/IdentitySwitch',
  component: () => import(/* webpackChunkName: "about" */ '../views/RecruitmentManage/IdentitySwitch.vue'), meta:{
    needLogin: true
  }
},
{//确已记录
  path: '/PurchaseRecord',
  component: () => import(/* webpackChunkName: "about" */ '../views/RecruitmentManage/PurchaseRecord.vue'), meta:{
    needLogin: true
  }
},{
  path: '/Models',
  component: () => import(/* webpackChunkName: "about" */ '../views/Models.vue'), meta:{
    needLogin: true
  }
},{
  path: '/Particulars_Models',
  component: () => import(/* webpackChunkName: "about" */ '../views/Particulars_Models.vue'), meta:{
    needLogin: true
  }
}
  ,{//线下认证模特
    path: '/offlinemt',
    component:offlinemt , meta:{
      needLogin: true
    }
  }
  ,{//线下认证模特
    path: '/AddGS',
    component:AddGS , meta:{
      needLogin: true
    }
  },
  {//线下认证公司
    path: '/offlinemtgs',
    component:offlinemtgs , meta:{
      needLogin: true
    }
  },
  {//线下认证助理
    path: '/offlinemtzl',
    component:offlinemtzl , meta:{
      needLogin: true
    }
  }
  ,{//详情公司
    path: '/particularsGS',
    component:particularsGS , meta:{
      needLogin: true
    }
  }
  ,{//添加招聘管理
    path: '/Addrecruitment',
    component: Addrecruitment , meta:{
      needLogin: true
    }
  }
  ,{//添加招聘管理
    path: '/showrecruitment',
    component: showrecruitment , meta:{
      needLogin: true
    }
  }
  ,{//顯示招聘庫
    path: '/ShowAuditrecruitment',
    component:ShowAuditrecruitment , meta:{
      needLogin: true
    }
  }
  ,{//显示详情的招聘库
    path: '/particulaAud',
    component:particulaAud , meta:{
      needLogin: true
    }
  }
  
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
