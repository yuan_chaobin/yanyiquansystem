import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Common from './Config/Common.vue'

Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.prototype.$COMMON=Common

//路由守卫
router.beforeEach((to, from, next)=>{
  //路由中设置的needLogin字段就在to当中 
  console.log(window.sessionStorage.loginSession);
  console.log(to.path)
  if(window.sessionStorage.loginSession)
  {
    //
    // console.log(to.path) //每次跳转的路径
    if(to.path == '/Login'){
      //登录状态下 访问login.vue页面 会跳到index.vue
      next({path: '/MenuSystem'});
    }else{
      next();
    }
  }else{
    // 如果没有session ,访问任何页面。都会进入到 登录页
    if (to.path == '/Login') { // 如果是登录页面的话，直接next() -->解决注销后的循环执行bug
      next();
    } else { // 否则 跳转到登录页面
      next({ path: '/Login' });
    }
  }
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')



